from pytube import YouTube, Channel, Playlist
import re
import os

OS_SLASH = '/' if os.name == 'posix' else '\\'
HEADERS = {'Content-Type': 'application/json', 'User-Agent': 'Mozilla'}


def download_videos(channel_url: str, directory: str) -> list:
    try:
        channel = Channel(channel_url)
    except:
        channel = Playlist(channel_url)
    print('Starting downloads')
    for video in channel.videos:
        filename = video.streams[0].title.replace('/', '').replace('\\', '') + '.mp4'
        print(f'Download {filename}')
        exists = os.path.exists('{directory}{OS_SLASH}{filename}')
        if exists:
            print('Video already exists. Skipping')
            continue
        try:
            video.streams.get_highest_resolution().download(output_path=directory, filename=filename)
        except Exception as e:
            print(e)
    print('All YouTube videos downloaded successfully.')


def main():
    channel_url = input('Please paste YouTube channel url: ')
    directory = input('Write directory name for channel videos (videos): ') or 'videos'
    download_videos(channel_url, directory)

if __name__ == '__main__':
    main()
    while True:
        want_next = input('Do you want to download videos from another channel? (Y/n): ') or 'Y'
        if want_next in ['Y', 'y', 'Yes', 'yes']:
            main()
        else:
            break
