## Authomatically downloader videos from YouTube channels

### How install
Please onstall latest Python3 from offical site for your platform https://www.python.org \
Install GIT or manually download project from GitLab https://gitlab.com/svmitin/youtube-downloader

Install Python3 libraries
```
pip install -U pip
pip install requests
pip install pytube
```

Run Python script
```
python3 youtube-downloader.py
```

### Solving problems
If you installing Python on Windows you need select add python path \
On many platforms you need use `python` instead `python3` \
In channels from YouTube select tab `Videos` before copy URL

author: svmitin (c) 2022
https://gitlab.com/svmitin
